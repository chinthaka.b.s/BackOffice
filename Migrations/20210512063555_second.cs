﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TodoApi.Migrations
{
    public partial class second : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "DepartmentDeptCode",
                table: "Employees",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "DeptCode",
                table: "Employees",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Employees_DepartmentDeptCode",
                table: "Employees",
                column: "DepartmentDeptCode");

            migrationBuilder.AddForeignKey(
                name: "FK_Employees_Departments_DepartmentDeptCode",
                table: "Employees",
                column: "DepartmentDeptCode",
                principalTable: "Departments",
                principalColumn: "DeptCode",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Employees_Departments_DepartmentDeptCode",
                table: "Employees");

            migrationBuilder.DropIndex(
                name: "IX_Employees_DepartmentDeptCode",
                table: "Employees");

            migrationBuilder.DropColumn(
                name: "DepartmentDeptCode",
                table: "Employees");

            migrationBuilder.DropColumn(
                name: "DeptCode",
                table: "Employees");
        }
    }
}
